﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using Task17;
using System.IO;


namespace Task18_Part2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
                
            CharComboBox.Items.Add("Warrior");
            CharComboBox.Items.Add("Wizard");
            CharComboBox.Items.Add("Thief");
            CBox1.Items.Add("Male");
            CBox1.Items.Add("Female");
        }
        public void FillListBox(CharacterClass rpgCharacter)
        {

            SummeryBox.Items.Add($"Name: {rpgCharacter.Name}");
            SummeryBox.Items.Add($"Hp: {rpgCharacter.Hp}");
            SummeryBox.Items.Add($"Gender: {rpgCharacter.Gender}");
            SummeryBox.Items.Add($"ArmorRating: {rpgCharacter.ArmorRating}");
            SummeryBox.Items.Add($"Energy: {rpgCharacter.Energy}");

           

        }
        // Method for writing to file
        public void WriteFile(CharacterClass rpgchar)
        {
            // Write to file
            string path = "CharacterInsertSQL.txt";
            // Delete the file if it exists.
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("SUMMARY");
                //  sw.WriteLine($"Name : {rpgchar.Name}, Type: {rpgchar.Hp}, " +
                // $"HP: {rpgchar.Gender}, Energy: {rpgchar.ArmorRating}, Armour rating: {rpgchar.Energy}");
                sw.Write("");
                sw.WriteLine("Inser Query");
                sw.WriteLine($"INSERT INTO Character(Type, Name, Hp, Energy, armorRating, gender) VALUES (' {rpgchar.GetType().Name}',' {rpgchar.Name}', ' {rpgchar.Hp}', " +
              $"'{rpgchar.Energy}', ' {rpgchar.ArmorRating}', '{rpgchar.Gender}')");
               
            }
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // Takes in name from input box
            String CharName = InsertNametxt.Text;

            // Takes in gender from input box
            String genderType1= CBox1.SelectedItem.ToString();
            // fill out summary

            if (CharComboBox.Text == "")
            {
                SummeryBox.Items.Add("Please fill out information to get summary");
            }
            else if (CharComboBox.SelectedItem.ToString() == "Warrior")
            {
                Warrior warrior = new Warrior(CharName, genderType1);
                FillListBox(warrior);
                WriteFile(warrior);
                AddToDatabase(warrior);
            }
            else if (CharComboBox.SelectedItem.ToString() == "Wizard")
            {
                Wizard wizard = new Wizard(CharName, genderType1);
                FillListBox(wizard);
                WriteFile(wizard);
                AddToDatabase(wizard);
            }
            else if (CharComboBox.SelectedItem.ToString() == "Thief")
            {
                Thief thief = new Thief(CharName, genderType1);
                FillListBox(thief);
                WriteFile(thief);
                AddToDatabase(thief);

            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            InsertNametxt.Clear();
            SummeryBox.Items.Clear();
        }

        ///****************
        ///
        public void AddToDatabase(CharacterClass rpgchar1) {
            using (SQLiteConnection mySqliteConn = CreateConnection())

            {
                //CreateTable(mySqliteConn);
                //Create operation
                
                UpdateData(mySqliteConn, rpgchar1);
                InsertData(mySqliteConn, rpgchar1);

                //Read Operations
                ReadData(mySqliteConn);
                mySqliteConn.Close();
            }
        }
        /////*********
        //
        public void EditDatabase(CharacterClass rpgchar1)
        {
            using (SQLiteConnection mySqliteConn = CreateConnection())

            {
                //CreateTable(mySqliteConn);
                //Create operation

                UpdateData(mySqliteConn, rpgchar1);
                //InsertData(mySqliteConn, rpgchar1);

                //Read Operations
                ReadData(mySqliteConn);
                mySqliteConn.Close();
            }
        }
        static SQLiteConnection CreateConnection()
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection("Data Source=Task15Database.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return sqlite_conn;
        }
        //region Data Definition Operations 
       
        //Read the data
        public void ReadData(SQLiteConnection conn)
        {
            SQLiteDataReader sqlite_datareader;

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM  Character";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                string myreader = sqlite_datareader.GetInt32(0) + " " + sqlite_datareader.GetString(1);
                list.Items.Add(myreader);
            }

        }
        public void InsertData(SQLiteConnection conn, CharacterClass rpgchar1)
        {
            SQLiteCommand sqlite_cmd;
            try
            {
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = $"INSERT INTO Character(Type, Name, Hp, Energy, armorRating, gender) VALUES (' {rpgchar1.GetType().Name}',' {rpgchar1.Name}', ' {rpgchar1.Hp}', '{rpgchar1.Energy}', ' {rpgchar1.ArmorRating}', '{rpgchar1.Gender}')";
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
        }
        public void UpdateData(SQLiteConnection conn, CharacterClass rpgchar1)
        {
            SQLiteCommand sqlite_cmd;
            try
            {
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = $"UPDATE Character SET (Type=' {rpgchar1.GetType().Name}'=, Name=' {rpgchar1.Name}', Hp=' {rpgchar1.Hp}', Energy=' {rpgchar1.Energy}', armorRating=' {rpgchar1.ArmorRating}', gender=' {rpgchar2.Gender}') WHERE (' {rpgchar1.GetType().Name}'=' {rpgchar2.GetType().Name}',' {rpgchar1.Name}'=' {rpgchar2.Name}', ' {rpgchar1.Hp}'={rpgchar2.Hp}', '{rpgchar1.Energy}'='{rpgchar2.Energy}', ' {rpgchar1.ArmorRating}'=' {rpgchar2.ArmorRating}', '{rpgchar1.Gender}'='{rpgchar2.Gender}')";
                // command.Text = "UPDATE Student SET Address = @add, City = @cit Where FirstName = @fn AND LastName = @ln";
                sqlite_cmd.CommandText = $"UPDATE Character SET (Type=' {rpgchar1.GetType().Name}'=, Name=' {rpgchar1.Name}', Hp=' {rpgchar1.Hp}', Energy=' {rpgchar1.Energy}', armorRating=' {rpgchar1.ArmorRating}', gender=' {rpgchar1.Gender}') WHERE (' {rpgchar1.GetType().Name}'= @Type', {rpgchar1.GetType().Name}'=' @Name',' {rpgchar1.Name}'=' {rpgchar1.Name}', ' {rpgchar1.Hp}'=@Hp', '{rpgchar1.Energy}'='@Energy', ' {rpgchar1.ArmorRating}'=' @ArmorRating', '{rpgchar1.Gender}'='@Gender')";
                
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private void CBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnDisplay_Click(object sender, EventArgs e)
        {
           

        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            // Takes in name from input box
            String CharName = InsertNametxt.Text;

            // Takes in gender from input box
            String genderType1 = CBox1.SelectedItem.ToString();
            // fill out summary

            if (CharComboBox.Text == "")
            {
                SummeryBox.Items.Add("Please fill out information to get summary");
            }
            else if (CharComboBox.SelectedItem.ToString() == "Warrior")
            {
                Warrior warrior = new Warrior(CharName, genderType1);
                FillListBox(warrior);
                WriteFile(warrior);
                EditDatabase(warrior);
            }
            else if (CharComboBox.SelectedItem.ToString() == "Wizard")
            {
                Wizard wizard = new Wizard(CharName, genderType1);
                FillListBox(wizard);
                WriteFile(wizard);
                EditDatabase(wizard);
               
            }
            else if (CharComboBox.SelectedItem.ToString() == "Thief")
            {
                Thief thief = new Thief(CharName, genderType1);
                FillListBox(thief);
                WriteFile(thief);
                EditDatabase(thief);

            }

        }

        public void Button3_Click(object sender, EventArgs e)
        {
           
        }

        private void InsertNametxt_TextChanged(object sender, EventArgs e)
        {

        }
        /*

*/
    }
}
