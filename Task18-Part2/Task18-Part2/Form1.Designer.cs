﻿namespace Task18_Part2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.InsertNametxt = new System.Windows.Forms.TextBox();
            this.CharComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SummeryBox = new System.Windows.Forms.ListBox();
            this.CBox1 = new System.Windows.Forms.ComboBox();
            this.list = new System.Windows.Forms.ListView();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter Gender";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Choose Character";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Summary";
            // 
            // InsertNametxt
            // 
            this.InsertNametxt.Location = new System.Drawing.Point(163, 14);
            this.InsertNametxt.Name = "InsertNametxt";
            this.InsertNametxt.Size = new System.Drawing.Size(121, 22);
            this.InsertNametxt.TabIndex = 4;
            this.InsertNametxt.TextChanged += new System.EventHandler(this.InsertNametxt_TextChanged);
            // 
            // CharComboBox
            // 
            this.CharComboBox.FormattingEnabled = true;
            this.CharComboBox.Location = new System.Drawing.Point(163, 108);
            this.CharComboBox.Name = "CharComboBox";
            this.CharComboBox.Size = new System.Drawing.Size(121, 24);
            this.CharComboBox.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 35);
            this.button1.TabIndex = 7;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(152, 192);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 35);
            this.button2.TabIndex = 8;
            this.button2.Text = "Delete All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // SummeryBox
            // 
            this.SummeryBox.FormattingEnabled = true;
            this.SummeryBox.ItemHeight = 16;
            this.SummeryBox.Location = new System.Drawing.Point(501, 61);
            this.SummeryBox.Name = "SummeryBox";
            this.SummeryBox.Size = new System.Drawing.Size(275, 100);
            this.SummeryBox.TabIndex = 9;
            // 
            // CBox1
            // 
            this.CBox1.FormattingEnabled = true;
            this.CBox1.Location = new System.Drawing.Point(163, 61);
            this.CBox1.Name = "CBox1";
            this.CBox1.Size = new System.Drawing.Size(121, 24);
            this.CBox1.TabIndex = 11;
            this.CBox1.SelectedIndexChanged += new System.EventHandler(this.CBox1_SelectedIndexChanged);
            // 
            // list
            // 
            this.list.HideSelection = false;
            this.list.Location = new System.Drawing.Point(501, 181);
            this.list.Name = "list";
            this.list.Size = new System.Drawing.Size(275, 185);
            this.list.TabIndex = 12;
            this.list.UseCompatibleStateImageBehavior = false;
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Location = new System.Drawing.Point(538, 393);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(92, 23);
            this.BtnUpdate.TabIndex = 14;
            this.BtnUpdate.Text = "Update";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnUpdate);
            this.Controls.Add(this.list);
            this.Controls.Add(this.CBox1);
            this.Controls.Add(this.SummeryBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CharComboBox);
            this.Controls.Add(this.InsertNametxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox InsertNametxt;
        private System.Windows.Forms.ComboBox CharComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox SummeryBox;
        private System.Windows.Forms.ComboBox CBox1;
        private System.Windows.Forms.ListView list;
        private System.Windows.Forms.Button BtnUpdate;
    }
}

